# Super Formatting (deprecated)

> since 3.193.5, Standard Notes implemented margins for all block elements

A quick toggle to adjust the margins of various elements within the Standard Notes Super Editor to help with readability. It should be available within the official plugins repository.

This Toggle is theme agnostic.

If you always want to use the latest version (I sometimes tend to adjust a few little things here and there), you can use my deployed version here:

https://marcaux.gitlab.io/sn-super-formatting/ext.json
