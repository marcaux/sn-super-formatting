#!/bin/sh
apt update
apt install zip
zip -r sn-super-formatting.zip ./ -x "package-lock.json" ".env" "*.sh" ".git*" ".git/*" "dist/.gitkeep" ".DS*" ".htaccess" "node_modules/*" "src/*" "*.txt"
mkdir public
mv sn-super-formatting.zip public/
cd public
unzip sn-super-formatting.zip
